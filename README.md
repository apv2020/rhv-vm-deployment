# rhv-vm-deployment

Playbook que crea vms basadas en RHE/Centos en base a un template en ambientes Red Hat Virtualization/Ovirt.

# Prerequisitos de software

El playbook utiliza el módulo de Ansible ovirt_vm para crear vms desde template, por lo que para su correcto funcionamiento, el nodo de control de
Ansible requiere las siguientes versiones de software:

 ansible =2.7
 python >= 2.7
 ovirt-engine-sdk-python >= 4.3.0

#  Prerequisitos de template

El template de RHEL/Centos debe tener instalado el paquete cloud-init antes de ser sellado ya que ip, password, hostname y contraseñas son seteadas 
a través de RHV/Ovirt utilizando cloud-init como método.

# Parametrización de variables

El archivo vars.yml contiene los siguientes valores para su funcionamiento:

 
**userrhv**: Usuario administrador de RHV/Ovirt (default: admin@internal ) <br>
**passwordrvh**: Password de RHV/Ovirt <br>
**urlrhv**:  Url de la api de RHV/Ovirt<br>
**templaterhv**: Nombre del template en el que se basarán las vms.<br>
**cluster**: Cluster de RHV/Ovirt que contiene el template <br>
**servers**: Diccionario de Ansible que contendrá los datos de las vm <br>

    
Ejemplo de archivo vars.yml:


```
userrhv: admin@internal 
passwordrvh: somepassword 
urlrhv: https://rhevm.example.local/ovirt-engine/api
templaterhv: rhel7-base-latest
cluster: CLUSTER1
servers:
  srv1:
    srvname: server1.rhlabs.local
    memoria: 8GiB
    cpu: 1
    ip_address: 192.168.1.10
    ip_net: 255.255.255.0
    ip_gateway: 192.168.1.1
    ip_nic: eth0
    os_user: root
    os_pass: somepassword
    dns: 192.168.1.1
  srv2:
    srvname: server2.rhlabs.local
    memoria: 8GiB
    cpu: 1
    ip_address: 192.168.1.11
    ip_net: 255.255.255.0
    ip_gateway: 192.168.1.1
    ip_nic: eth0
    os_user: root
    os_pass: somepassword
    dns: 192.168.1.1
```
    
    # Uso
    
    ansible-playbook rhev-deploy-vms.yml
    
    



